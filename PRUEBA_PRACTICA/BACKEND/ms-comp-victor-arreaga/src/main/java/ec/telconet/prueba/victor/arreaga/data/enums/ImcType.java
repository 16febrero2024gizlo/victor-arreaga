package ec.telconet.prueba.victor.arreaga.data.enums;

public enum ImcType {
    NORMAL,
    OVERWEIGHT,
    OBESITY_1,
    OBESITY_2,
    OBESITY_3;
}
