package ec.telconet.prueba.victor.arreaga.services.user;

import ec.telconet.prueba.victor.arreaga.api.dtos.UserDto;
import ec.telconet.prueba.victor.arreaga.api.dtos.auth.RegisterRequestDto;
import ec.telconet.prueba.victor.arreaga.data.daos.IImcRepository;
import ec.telconet.prueba.victor.arreaga.data.daos.IUserRepository;
import ec.telconet.prueba.victor.arreaga.data.entities.Imc;
import ec.telconet.prueba.victor.arreaga.data.entities.User;
import ec.telconet.prueba.victor.arreaga.data.enums.ImcType;
import ec.telconet.prueba.victor.arreaga.data.utils.Mapper;
import ec.telconet.prueba.victor.arreaga.services.exceptions.MessageException;
import ec.telconet.prueba.victor.arreaga.services.exceptions.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserService implements IUserService {
    private final IUserRepository userRepository;
    private final IImcRepository imcRepository;
    @Override
    public UUID save(RegisterRequestDto registerRequestDto) {
        User user = userRepository.findByUsername(registerRequestDto.username()).orElseThrow(() -> new NotFoundException(MessageException.USERNAME_ALREADY_EXISTS));
        return userRepository.save(user).getId();
    }

    @Override
    public UUID update(UUID userId, UserDto userDto) {
        User user = userRepository.findById(userId).orElseThrow(() -> new NotFoundException(MessageException.USER_NOT_FOUND));
        Imc imc = imcRepository.findByUserId(userId).orElseThrow(() -> new NotFoundException(MessageException.IMC_NOT_FOUND));
        imc.calculate();
        this.imcRepository.save(imc);
        return userRepository.save(user).getId();
    }

    @Override
    public void deleteById(UUID userId) {
        userRepository.deleteById(userId);
    }
}
