package ec.telconet.prueba.victor.arreaga.services.imc;

import ec.telconet.prueba.victor.arreaga.api.dtos.FilterImcDto;
import ec.telconet.prueba.victor.arreaga.api.dtos.ImcDto;
import ec.telconet.prueba.victor.arreaga.api.dtos.UserDto;
import ec.telconet.prueba.victor.arreaga.data.daos.IImcRepository;
import ec.telconet.prueba.victor.arreaga.data.entities.Imc;
import ec.telconet.prueba.victor.arreaga.data.entities.User;
import ec.telconet.prueba.victor.arreaga.data.utils.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ImcService implements IImcService {

    private final IImcRepository iImcRepository;
    private final Mapper mapper;

    @Override
    public UUID update(UUID imcId, ImcDto imcDto) {
        return null;
    }

    @Override
    public List<ImcDto> findAll(FilterImcDto filterImcDto) {
        return this.mapper.toDtos(this.iImcRepository.findAllByFilter(filterImcDto.getRecipe(), filterImcDto.getImcType(), filterImcDto.getRiskType()), ImcDto.class);
    }


    @Override
    public List<UserDto> findAllFromUsersByFilter(FilterImcDto filterImcDto) {
        List<Imc> imcs = this.iImcRepository.findAllByFilter(filterImcDto.getRecipe(), filterImcDto.getImcType(), filterImcDto.getRiskType());
        List<User> users = Arrays.asList(imcs.stream().map(Imc::getUser).toArray(User[]::new));
        return this.mapper.toDtos(users, UserDto.class);
    }

    @Override
    public List<ImcDto> findAll() {
        return this.mapper.toDtos(this.iImcRepository.findAll(), ImcDto.class);
    }

    @Override
    public List<UserDto> findAllFromUsers() {
        List<Imc> imcs = this.iImcRepository.findAll();
        List<User> users = Arrays.asList(imcs.stream().map(Imc::getUser).toArray(User[]::new));
        return this.mapper.toDtos(users, UserDto.class);
    }
}
