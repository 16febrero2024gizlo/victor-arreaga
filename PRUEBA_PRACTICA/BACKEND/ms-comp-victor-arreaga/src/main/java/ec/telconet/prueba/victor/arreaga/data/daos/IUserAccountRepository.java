package ec.telconet.prueba.victor.arreaga.data.daos;


import ec.telconet.prueba.victor.arreaga.data.entities.identity.UserAccount;

import java.util.Optional;
import java.util.UUID;

public interface IUserAccountRepository extends IRepository<UserAccount, UUID> {
    Optional<UserAccount> findByUsername(String username);
}
