package ec.telconet.prueba.victor.arreaga.services.exceptions;

public class BadRequestException extends RuntimeException {
    public BadRequestException(MessageException messageException) {
        super(messageException.getMessage());
    }

}
