package ec.telconet.prueba.victor.arreaga;

import ec.telconet.prueba.victor.arreaga.configurations.RsaKeys;
import ec.telconet.prueba.victor.arreaga.data.daos.IUserRoleRepository;
import ec.telconet.prueba.victor.arreaga.data.entities.identity.UserRole;
import ec.telconet.prueba.victor.arreaga.data.enums.Role;
import ec.telconet.prueba.victor.arreaga.services.useraccount.UserAccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing(auditorAwareRef = "auditorAware")
@EnableConfigurationProperties(RsaKeys.class)
@RequiredArgsConstructor
public class MsCompVictorArreagaApplication implements ApplicationRunner {
    private final UserAccountService userAccountService;
    private final IUserRoleRepository userRoleRepository;

    public static void main(String[] args) {
        SpringApplication.run(MsCompVictorArreagaApplication.class, args);
    }


    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (this.userRoleRepository.count() == 0) {
            UserRole userRole = new UserRole();
            userRole.setName(Role.ROLE_ADMIN);
            this.userRoleRepository.save(userRole);
            userRole = new UserRole();
            userRole.setName(Role.ROLE_AUTHENTICATED);
            this.userRoleRepository.save(userRole);
            userAccountService.setUsersFromDummy();
        }
    }


}
