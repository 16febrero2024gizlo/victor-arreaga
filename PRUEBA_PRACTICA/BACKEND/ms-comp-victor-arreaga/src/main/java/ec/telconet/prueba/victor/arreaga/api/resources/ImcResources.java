package ec.telconet.prueba.victor.arreaga.api.resources;

import ec.telconet.prueba.victor.arreaga.api.dtos.FilterImcDto;
import ec.telconet.prueba.victor.arreaga.api.dtos.ImcDto;
import ec.telconet.prueba.victor.arreaga.api.dtos.UserDto;
import ec.telconet.prueba.victor.arreaga.data.enums.ImcType;
import ec.telconet.prueba.victor.arreaga.data.enums.RiskType;
import ec.telconet.prueba.victor.arreaga.services.imc.IImcService;
import lombok.RequiredArgsConstructor;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/imcs")
public class ImcResources {
    private final IImcService iImcService;

    @GetMapping("search/")
    ResponseEntity<List<ImcDto>> findAll(@RequestParam String recipe, @RequestParam ImcType imcType, @RequestParam RiskType riskType) {
        FilterImcDto filterImcDto = new FilterImcDto();
        filterImcDto.setRecipe(recipe);
        filterImcDto.setImcType(imcType);
        filterImcDto.setRiskType(riskType);
        return ResponseEntity.ok(this.iImcService.findAll(filterImcDto));
    }

    @GetMapping
    ResponseEntity<List<ImcDto>> findAll() {
        return ResponseEntity.ok(this.iImcService.findAll());
    }

    @PutMapping("{imcId}")
    ResponseEntity<HttpHeader> update(@PathVariable UUID imcId, @RequestBody ImcDto imcDto) {
        UUID id = this.iImcService.update(imcId, imcDto);
        return new ResponseEntity<>(HttpHeader.getHttpHeaders(id), HttpStatus.OK);

    }
}