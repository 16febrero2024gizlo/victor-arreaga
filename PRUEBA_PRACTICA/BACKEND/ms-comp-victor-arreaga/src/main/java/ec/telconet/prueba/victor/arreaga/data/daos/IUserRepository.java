package ec.telconet.prueba.victor.arreaga.data.daos;


import ec.telconet.prueba.victor.arreaga.data.entities.User;

import java.util.Optional;
import java.util.UUID;

public interface IUserRepository extends IRepository<User, UUID> {

    Optional<User> findByUsername(String username);
}