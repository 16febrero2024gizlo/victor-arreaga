package ec.telconet.prueba.victor.arreaga.api.dtos.auth;


public record LoginRequestDto(
        String username,
        String password
) {
}