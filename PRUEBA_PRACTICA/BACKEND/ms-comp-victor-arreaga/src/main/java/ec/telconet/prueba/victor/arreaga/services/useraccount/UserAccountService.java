package ec.telconet.prueba.victor.arreaga.services.useraccount;


import ec.telconet.prueba.victor.arreaga.api.dtos.dummy.DummyDto;
import ec.telconet.prueba.victor.arreaga.api.dtos.dummy.UserDummyDto;
import ec.telconet.prueba.victor.arreaga.data.daos.IUserAccountRepository;
import ec.telconet.prueba.victor.arreaga.data.daos.IUserRepository;
import ec.telconet.prueba.victor.arreaga.data.daos.IUserRoleRepository;
import ec.telconet.prueba.victor.arreaga.data.entities.User;
import ec.telconet.prueba.victor.arreaga.data.entities.identity.UserAccount;
import ec.telconet.prueba.victor.arreaga.data.entities.identity.UserRole;
import ec.telconet.prueba.victor.arreaga.data.enums.Role;
import ec.telconet.prueba.victor.arreaga.services.exceptions.MessageException;
import ec.telconet.prueba.victor.arreaga.services.exceptions.NotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserAccountService implements IUserAccountService {

    private final IUserAccountRepository userAccountRepository;
    private final IUserRoleRepository userRoleRepository;
    private final IUserRepository userRepository;
    private final RestTemplate restTemplate;
    private final PasswordEncoder passwordEncoder;


    public void setUsersFromDummy() {
        List<UserDummyDto> dummyUsers = fetchDummyUsers();
        for (UserDummyDto userDummyDto : dummyUsers) {
            processDummyUser(userDummyDto);
        }
    }

    private List<UserDummyDto> fetchDummyUsers() {
        final String END_POINT_USER = "https://dummyjson.com/users?limit=5";
        DummyDto valuesOfDummy = restTemplate.getForObject(END_POINT_USER, DummyDto.class);
        return Objects.requireNonNull(valuesOfDummy).getUsers();
    }

    private void processDummyUser(UserDummyDto userDummyDto) {
        Optional<UserAccount> userAccountOptional = userAccountRepository.findByUsername(userDummyDto.getUsername());
        if (userAccountOptional.isPresent()) {
            log.info("User {} already exists", userDummyDto.getUsername());
        } else {
            log.info("User {} not exists, creating", userDummyDto.getUsername());
            saveUserAccount(userDummyDto);
            saveUser(userDummyDto);
        }
    }
    private void saveUser(UserDummyDto userDummyDto) {
        User user = User.builder()
                .firstName(userDummyDto.getFirstName())
                .lastName(userDummyDto.getLastName())
                .username(userDummyDto.getUsername())
                .age(userDummyDto.getAge())
                .email(userDummyDto.getEmail())
                .image(userDummyDto.getImage())
                .height(userDummyDto.getHeight())
                .weight(userDummyDto.getWeight())
                .createdBy("ADMIN")
                .build();
        userRepository.save(user);
    }

    private void saveUserAccount(UserDummyDto userDummyDto) {
        UserAccount userAccount = new UserAccount();
        userAccount.setUsername(userDummyDto.getUsername());
        userAccount.setEmail(userDummyDto.getEmail());
        userAccount.setPassword( passwordEncoder.encode(userDummyDto.getPassword()));

        Set<Role> roles = new HashSet<>();
        roles.add(Role.ROLE_ADMIN);
        roles.add(Role.ROLE_AUTHENTICATED);

        Set<UserRole> userRoles = new HashSet<>();
        for (Role role : roles) {
            userRoles.add(getUserRole(role));
        }
        userAccount.setRoles(userRoles);

        userAccountRepository.save(userAccount);
    }


    private UserRole getUserRole(Role role) {
        return userRoleRepository.findByName(role).orElseThrow(() -> new NotFoundException(MessageException.ROLE_NOT_FOUND));
    }
}
