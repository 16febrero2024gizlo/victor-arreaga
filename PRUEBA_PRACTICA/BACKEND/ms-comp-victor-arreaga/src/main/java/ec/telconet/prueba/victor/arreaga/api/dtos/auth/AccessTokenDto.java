package ec.telconet.prueba.victor.arreaga.api.dtos.auth;

public record AccessTokenDto(String token) {
}
