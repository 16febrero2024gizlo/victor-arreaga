package ec.telconet.prueba.victor.arreaga.api.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import ec.telconet.prueba.victor.arreaga.data.entities.User;
import ec.telconet.prueba.victor.arreaga.data.enums.ImcType;
import ec.telconet.prueba.victor.arreaga.data.enums.RiskType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ImcDto {
    UUID id;
    ImcType imcType;
    RiskType riskType;
    String recipes;
    float value;
    User user;
}
