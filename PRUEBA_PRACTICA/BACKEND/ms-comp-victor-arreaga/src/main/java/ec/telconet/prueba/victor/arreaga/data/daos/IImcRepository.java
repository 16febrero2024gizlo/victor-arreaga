package ec.telconet.prueba.victor.arreaga.data.daos;

import ec.telconet.prueba.victor.arreaga.data.entities.Imc;
import ec.telconet.prueba.victor.arreaga.data.enums.ImcType;
import ec.telconet.prueba.victor.arreaga.data.enums.RiskType;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface IImcRepository extends IRepository<Imc, UUID> {
    Optional<Imc> findByUserId(UUID userId);

    @Query("SELECT i from Imc i where i.recipe = ?1 and i.imcType = ?2 and i.riskType = ?3")
    List<Imc> findAllByFilter(String recipe, ImcType imcType, RiskType riskType);
}
