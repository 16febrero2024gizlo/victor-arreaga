package ec.telconet.prueba.victor.arreaga.data.daos;


import ec.telconet.prueba.victor.arreaga.data.entities.identity.UserRole;
import ec.telconet.prueba.victor.arreaga.data.enums.Role;

import java.util.Optional;
import java.util.UUID;

public interface IUserRoleRepository extends IRepository<UserRole, UUID> {

    Optional<UserRole> findByName(Role name);

}
