package ec.telconet.prueba.victor.arreaga.data.entities;


import ec.telconet.prueba.victor.arreaga.data.entities.auditing.Auditing;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Size;
import lombok.*;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@SuperBuilder
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@EntityListeners(AuditingEntityListener.class)
@Entity
@Table(name = "users")
public class User extends Auditing {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    UUID id;

    @Size(max = 15)
    @Column(nullable = false, length = 15)
    String username;

    @Size(max = 50)
    @Column(nullable = false, length = 50)
    String firstName;

    @Size(max = 50)
    @Column(nullable = false, length = 50)
    String lastName;

    Short age;

    @Email
    @Size(max = 100)
    @Column(nullable = false, length = 100)
    String email;

    String image;

    float height;

    float weight;

    @OneToMany(mappedBy = "user")
    List<Imc> imcs = new ArrayList<>();

    public String getNames() {
        return this.firstName + " " + this.lastName;
    }
}