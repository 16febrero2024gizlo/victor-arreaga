package ec.telconet.prueba.victor.arreaga.data.entities;

import ec.telconet.prueba.victor.arreaga.data.enums.ImcType;
import ec.telconet.prueba.victor.arreaga.data.enums.RiskType;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.util.UUID;

@SuperBuilder
@Getter
@Setter
@ToString
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@EntityListeners(AuditingEntityListener.class)
@Entity
@Table(name = "imcs")
public class Imc {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    UUID id;

    @Enumerated(EnumType.STRING)
    ImcType imcType;

    @Enumerated(EnumType.STRING)
    RiskType riskType;

    @Column(nullable = false, length = 100)
    String recipe;

    @Column(nullable = false)
    float value;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id")
    User user;


    public void calculate() {
        float imc = user.getWeight() / (user.getHeight() * user.getHeight());
        if (imc >= 18.5 && imc < 24.9) {
            this.imcType = ImcType.NORMAL;
            this.riskType = RiskType.AVERAGE;
        } else if (imc >= 25 && imc < 29.9) {
            this.imcType = ImcType.OVERWEIGHT;
            this.riskType = RiskType.INCREASED;
        } else if (imc >= 30 && imc < 34.9) {
            this.imcType = ImcType.OBESITY_1;
            this.riskType = RiskType.MODERATE;
        } else if (imc >= 35 && imc < 39.9) {
            this.imcType = ImcType.OBESITY_2;
            this.riskType = RiskType.SEVERE;
        } else {
            this.imcType = ImcType.OBESITY_3;
            this.riskType = RiskType.VERY_SEVERE;
        }
        this.value = imc;
    }
}
