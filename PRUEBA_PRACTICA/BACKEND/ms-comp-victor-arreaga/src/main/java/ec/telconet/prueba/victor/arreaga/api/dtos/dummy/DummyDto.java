package ec.telconet.prueba.victor.arreaga.api.dtos.dummy;

import lombok.Data;

import java.util.List;
@Data
public class DummyDto {
    List<UserDummyDto> users;
}
