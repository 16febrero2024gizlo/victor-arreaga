package ec.telconet.prueba.victor.arreaga.api.dtos.auth;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Size;

public record RegisterRequestDto(
        @Size(max = 30)
        String firstName,
        @Size(max = 30)
        String lastName,
        @Size(max = 10)
        String username,
        @Email
        String email,
        String password

) {
}
