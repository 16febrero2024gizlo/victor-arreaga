package ec.telconet.prueba.victor.arreaga.api.resources;

import ec.telconet.prueba.victor.arreaga.api.dtos.FilterImcDto;
import ec.telconet.prueba.victor.arreaga.api.dtos.UserDto;
import ec.telconet.prueba.victor.arreaga.api.dtos.auth.RegisterRequestDto;
import ec.telconet.prueba.victor.arreaga.data.enums.ImcType;
import ec.telconet.prueba.victor.arreaga.data.enums.RiskType;
import ec.telconet.prueba.victor.arreaga.services.imc.IImcService;
import ec.telconet.prueba.victor.arreaga.services.user.IUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/users")
public class UserResource {

    private final IUserService userService;
    private final IImcService iImcService;

    @PostMapping
    ResponseEntity<HttpHeaders> save(@RequestBody RegisterRequestDto registerRequestDto) {
        UUID userId = userService.save(registerRequestDto);
        return new ResponseEntity<>(HttpHeader.getHttpHeaders(userId), HttpStatus.CREATED);
    }

    @PutMapping("{userId}")
    ResponseEntity<HttpHeaders> update(@PathVariable UUID userId, @RequestBody UserDto userDto) {
        UUID articleId = userService.update(userId, userDto);
        return new ResponseEntity<>(HttpHeader.getHttpHeaders(articleId), HttpStatus.OK);
    }

    @DeleteMapping("{userId}")
    ResponseEntity<Void> delete(@PathVariable UUID userId) {
        userService.deleteById(userId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


    @GetMapping("search/imcs")
    ResponseEntity<List<UserDto>> findAllFromUsersByFilter(@RequestParam String recipe, @RequestParam ImcType imcType, @RequestParam RiskType riskType) {
        FilterImcDto filterImcDto = new FilterImcDto();
        filterImcDto.setRecipe(recipe);
        filterImcDto.setImcType(imcType);
        filterImcDto.setRiskType(riskType);
        return ResponseEntity.ok(this.iImcService.findAllFromUsersByFilter(filterImcDto));
    }

    @GetMapping("imcs")
    ResponseEntity<List<UserDto>> findAllFromUsers() {
        return ResponseEntity.ok(this.iImcService.findAllFromUsers());
    }

}
