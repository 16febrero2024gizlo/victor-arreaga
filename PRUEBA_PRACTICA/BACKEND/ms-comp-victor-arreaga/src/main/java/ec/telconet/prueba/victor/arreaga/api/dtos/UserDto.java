package ec.telconet.prueba.victor.arreaga.api.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import ec.telconet.prueba.victor.arreaga.data.enums.Role;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserDto {
    UUID id;
    String username;
    String firstname;
    String lastname;
    Short age;
    String email;
    String image;
    float height;
    float weight;
    List<ImcDto> imcs;
}
