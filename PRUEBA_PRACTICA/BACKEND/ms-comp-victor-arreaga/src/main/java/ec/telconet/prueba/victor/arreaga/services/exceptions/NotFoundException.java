package ec.telconet.prueba.victor.arreaga.services.exceptions;

public class NotFoundException extends RuntimeException {
    public NotFoundException(MessageException messageException) {
        super(messageException.getMessage());
    }
}
