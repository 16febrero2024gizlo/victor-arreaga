package ec.telconet.prueba.victor.arreaga.data.enums;

public enum RiskType {
    AVERAGE,
    SEVERE,
    INCREASED,
    MODERATE,
    VERY_SEVERE
}
