package ec.telconet.prueba.victor.arreaga.services.user;

import ec.telconet.prueba.victor.arreaga.api.dtos.UserDto;
import ec.telconet.prueba.victor.arreaga.api.dtos.auth.RegisterRequestDto;
import ec.telconet.prueba.victor.arreaga.data.enums.ImcType;

import java.util.List;
import java.util.UUID;

public interface IUserService {
    UUID save(RegisterRequestDto registerRequestDto);


    UUID update(UUID userId, UserDto userDto);

    void deleteById(UUID userId);
}
