package ec.telconet.prueba.victor.arreaga.services.imc;

import ec.telconet.prueba.victor.arreaga.api.dtos.FilterImcDto;
import ec.telconet.prueba.victor.arreaga.api.dtos.ImcDto;
import ec.telconet.prueba.victor.arreaga.api.dtos.UserDto;

import java.util.List;
import java.util.UUID;

public interface IImcService {

    UUID update(UUID imcId, ImcDto imcDto);

    List<ImcDto> findAll(FilterImcDto filterImcDto);

    List<UserDto> findAllFromUsersByFilter(FilterImcDto filterImcDto);

    List<ImcDto> findAll();

    List<UserDto> findAllFromUsers();
}
