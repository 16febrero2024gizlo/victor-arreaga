package ec.telconet.prueba.victor.arreaga.services.exceptions;

public class ForbiddenException extends RuntimeException {
    public ForbiddenException(MessageException messageException) {
        super(messageException.getMessage());
    }

}
