package ec.telconet.prueba.victor.arreaga.api.dtos.dummy;

import lombok.Data;

@Data
public class UserDummyDto {
    int id;
    String firstName;
    String lastName;
    Short age;
    String email;
    String image;
    float height;
    float weight;
    String username;
    String password;
}
