package ec.telconet.prueba.victor.arreaga.services.auth;

import ec.telconet.prueba.victor.arreaga.api.dtos.auth.AccessTokenDto;
import ec.telconet.prueba.victor.arreaga.api.dtos.auth.LoginRequestDto;
import ec.telconet.prueba.victor.arreaga.api.dtos.auth.RegisterRequestDto;
import ec.telconet.prueba.victor.arreaga.data.daos.IUserAccountRepository;
import ec.telconet.prueba.victor.arreaga.data.daos.IUserRepository;
import ec.telconet.prueba.victor.arreaga.data.daos.IUserRoleRepository;
import ec.telconet.prueba.victor.arreaga.data.entities.User;
import ec.telconet.prueba.victor.arreaga.data.entities.identity.UserAccount;
import ec.telconet.prueba.victor.arreaga.data.entities.identity.UserRole;
import ec.telconet.prueba.victor.arreaga.data.enums.Role;
import ec.telconet.prueba.victor.arreaga.services.exceptions.MessageException;
import ec.telconet.prueba.victor.arreaga.services.exceptions.NotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.UUID;

@Component
@RequiredArgsConstructor
@Slf4j
public class AuthService {

    private final AuthenticationManager authenticationManager;
    private final JwtAccessTokenService jwtAccessTokenService;
    private final IUserAccountRepository accountRepository;
    private final PasswordEncoder passwordEncoder;
    private final IUserRoleRepository userRoleRepository;
    private final IUserRepository userRepository;

    public AccessTokenDto authenticate(LoginRequestDto loginRequestDto) {
        log.info("Authenticating user: {}", loginRequestDto);
        Authentication authenticationToken = new UsernamePasswordAuthenticationToken(
                loginRequestDto.username(),
                loginRequestDto.password()
        );
        Authentication authentication = this.authenticationManager.authenticate(authenticationToken);

        String token = this.jwtAccessTokenService.generateToken(authentication);
        return new AccessTokenDto(token);
    }

    public UUID register(RegisterRequestDto registerRequestDto) {
        this.checkIfUsernameExists(registerRequestDto.username());
        saveUserAccount(registerRequestDto);
        return saveUser(registerRequestDto);
    }

    private UUID saveUser(RegisterRequestDto registerRequestDto) {
        return this.userRepository.save(
                User.builder()
                        .firstName(registerRequestDto.firstName())
                        .lastName(registerRequestDto.lastName())
                        .email(registerRequestDto.email())
                        .username(registerRequestDto.username())
                        .build()
        ).getId();
    }

    private void saveUserAccount(RegisterRequestDto registerRequestDto) {
        Set<UserRole> roleAuthenticated = Set.of(getRoleAuthenticated());
        this.accountRepository.save(
                UserAccount.builder()
                        .username(registerRequestDto.username())
                        .password(registerRequestDto.password())
                        .password(passwordEncoder.encode(registerRequestDto.password()))
                        .email(registerRequestDto.email())
                        .roles(roleAuthenticated)
                        .build()
        );
    }

    private UserRole getRoleAuthenticated() {
        return this.userRoleRepository.findByName(Role.ROLE_AUTHENTICATED)
                .orElseThrow(() -> new NotFoundException(MessageException.ROLE_NOT_FOUND));
    }

    private void checkIfUsernameExists(String username) {
        boolean isUsernameExists = accountRepository.findByUsername(username).isPresent();
        if (isUsernameExists) {
            throw new NotFoundException(MessageException.USERNAME_ALREADY_EXISTS);
        }
    }
}
