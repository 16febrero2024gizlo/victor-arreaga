import { ImcDto } from '@core/dtos/imc.dto';

export interface UserDto {
  id: string;
  username: string;
  firstName: string;
  lastName: string;
  age: number;
  email: string;
  image: string;
  height: number;
  weight: number;
  imc: ImcDto;
  createdAt: string;
}
