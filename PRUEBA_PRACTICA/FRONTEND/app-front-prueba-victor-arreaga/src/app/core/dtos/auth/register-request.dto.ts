export interface RegisterRequestDto {
  firstName: string;
  lastName: string;
  email: string;
  username: string;
  password: string;
}
