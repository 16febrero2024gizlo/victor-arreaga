import { UserDto } from '@core/dtos/user.dto';
import { ImcCardDto } from '@core/dtos/imc-card.dto';

export class UserImcCardDto {
  image: string;
  names: string;
  weight: number;
  height: number;
  imcCard: ImcCardDto;

  constructor(userDto: UserDto) {
    this.image = userDto.image;
    this.names = userDto.firstName + ' ' + userDto.lastName;
    this.weight = userDto.weight;
    this.height = userDto.height;
    this.imcCard = new ImcCardDto(userDto.imc);
  }
}
