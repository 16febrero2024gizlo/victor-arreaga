import { UserDto } from '@core/dtos/user.dto';

export interface ImcDto {
  imcType: string;
  riskType: string;
  recipe: string;
  value: number;
  user: UserDto;
  date: string;
}
