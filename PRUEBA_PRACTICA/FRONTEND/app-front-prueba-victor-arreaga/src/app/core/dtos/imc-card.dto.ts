import { ImcTypeEnum } from '@core/enums/imc-type-enum';
import { RiskTypeEnum } from '@core/enums/risk-type-enum';
import { ParseDate } from '@core/utils/parse-date';
import { ImcDto } from '@core/dtos/imc.dto';
import { getKey } from '@core/utils/enum.util';

export class ImcCardDto {
  recipe: string;
  imcType: ImcTypeEnum;
  riskType: RiskTypeEnum;
  date: string;
  value: number;

  constructor(imcDto: ImcDto) {
    this.recipe = imcDto.recipe;
    this.imcType = getKey(
      ImcTypeEnum,
      imcDto.imcType as ImcTypeEnum
    ) as ImcTypeEnum;
    this.riskType = getKey(
      RiskTypeEnum,
      imcDto.riskType as RiskTypeEnum
    ) as RiskTypeEnum;
    this.date = ParseDate.toRelativeTime(imcDto.date);
    this.value = imcDto.value;
  }
}
