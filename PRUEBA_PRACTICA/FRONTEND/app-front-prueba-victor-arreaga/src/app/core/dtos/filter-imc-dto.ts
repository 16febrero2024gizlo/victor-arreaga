import { ImcTypeEnum } from '@core/enums/imc-type-enum';
import { RiskTypeEnum } from '@core/enums/risk-type-enum';

export interface FilterImcDto {
  recipe: string;
  imcType: ImcTypeEnum;
  riskType: RiskTypeEnum;
}
