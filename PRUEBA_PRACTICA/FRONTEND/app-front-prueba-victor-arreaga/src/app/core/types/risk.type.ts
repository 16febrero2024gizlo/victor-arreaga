export type RiskType =
  | 'AVERAGE'
  | 'SEVERE'
  | 'INCREASED'
  | 'MODERATE'
  | 'VERY_SEVERE';
