import { UserDto } from '@core/dtos/user.dto';
import { Role } from '@core/types/role.type';

export interface Session extends UserDto {
  names: string;
  roles: Array<Role>;
  token: string;
}
