export enum ImcTypeEnum {
  Normal = 'Normal',
  Overweight = 'Sobrepeso',
  Obesity_1 = 'Obesidad tipo 1',
  Obesity_2 = 'Obesidad tipo 2',
  Obesity_3 = 'Obesidad tipo 3',
}