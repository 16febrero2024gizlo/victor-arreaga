export enum RiskTypeEnum {
  Average = 'Promedio',
  Severe = 'Severo',
  Increased = 'Aumentado',
  Moderate = 'Moderado',
  Very_severe = 'Muy severo',
}
