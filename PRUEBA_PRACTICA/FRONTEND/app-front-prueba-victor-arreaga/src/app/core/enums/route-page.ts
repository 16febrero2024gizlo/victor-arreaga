export enum RoutePage {
  Home = '',
  Auth = 'auth',
  AuthLogin = 'auth/login',
  AuthRegister = 'auth/register',
  Login = 'login',
  Register = 'register',
  Profile = 'profile',
  Article = 'article',
}
