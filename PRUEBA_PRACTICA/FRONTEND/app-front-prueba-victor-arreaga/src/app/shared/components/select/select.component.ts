import { Component, inject, Input, OnDestroy, OnInit } from '@angular/core';
import {
  ControlContainer,
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AngularSvgIconModule } from 'angular-svg-icon';

@Component({
  standalone: true,
  selector: 'app-select',
  imports: [CommonModule, ReactiveFormsModule, AngularSvgIconModule],
  templateUrl: './select.component.html',
  viewProviders: [
    {
      provide: ControlContainer,
      useFactory: () => inject(ControlContainer, { skipSelf: true }),
    },
  ],
})
export class SelectComponent implements OnInit, OnDestroy {
  @Input() options!: string[];
  @Input() optionSelected!: string;
  @Input() title!: string;
  @Input() label!: string;
  @Input() formGroupName!: string;
  categoryIncludes!: string;
  formSelect: FormGroup;
  parentContainer: ControlContainer;

  constructor() {
    this.formSelect = new FormGroup({});
    this.parentContainer = inject(ControlContainer);
  }

  get withTitle(): boolean {
    return this.title !== '';
  }

  get withIncludes(): boolean {
    return this.categoryIncludes !== '';
  }

  ngOnInit(): void {
    if (!this.optionSelected) {
      this.optionSelected = this.options[0];
    }
    this.formSelect = new FormGroup({
      select: new FormControl(this.optionSelected, [Validators.required]),
    });

    this.getControl().addControl(this.formGroupName, this.formSelect);
  }

  ngOnDestroy() {
    this.getControl().removeControl(this.formGroupName);
  }

  private getControl() {
    return this.parentContainer.control as FormGroup;
  }
}
