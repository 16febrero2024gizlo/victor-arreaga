import { inject, Injectable } from '@angular/core';
import { environment } from '@env/environment.development';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '@shared/services/auth.service';
import { UserDto } from '@core/dtos/user.dto';
import { FilterImcDto } from '@core/dtos/filter-imc-dto';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private readonly API_URL: string = environment.apiBaseUrl + '/users';
  private http: HttpClient = inject(HttpClient);
  private readonly authService: AuthService = inject(AuthService);

  editProfile(userDto: UserDto) {
    return this.http.put<void>(
      `${this.API_URL}/${this.authService.getUserId()}`,
      userDto,
      this.getToken()
    );
  }

  searchUsers(filterImc: FilterImcDto) {
    return this.http.get<UserDto[]>(
      `${this.API_URL}/search/users?title=${filterImc.recipe}&imcType=${filterImc.imcType}&riskType=${filterImc.riskType}`,
      this.getToken()
    );
  }

  getAllUsers() {
    return this.http.get<UserDto[]>(`${this.API_URL}/users`, this.getToken());
  }

  private getToken() {
    return {
      headers: {
        Authorization: `Bearer ${this.authService.getToken()}`,
      },
    };
  }
}
