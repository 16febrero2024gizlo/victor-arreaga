import { inject, Injectable } from '@angular/core';

import { Service } from '@shared/services/service';
import { ImcCardDto } from '@core/dtos/imc-card.dto';
import { ImcService } from '@shared/services/imc.service';
import { FilterImcDto } from '@core/dtos/filter-imc-dto';
import { map } from 'rxjs';
import { ImcDto } from '@core/dtos/imc.dto';

@Injectable({
  providedIn: 'root',
})
export class ExploreImcService extends Service<ImcCardDto[]> {
  private readonly imcService: ImcService = inject(ImcService);

  constructor() {
    super([] as ImcCardDto[]);
  }

  get totalImcCards(): number {
    return this.imcCards.length;
  }

  get imcCards() {
    return this.result();
  }

  search(filterImc: FilterImcDto): void {
    const request = this.imcService.search(filterImc).pipe(
      map((imcs: ImcDto[]) => {
        return imcs.map((imc: ImcDto) => {
          return new ImcCardDto(imc);
        });
      })
    );
    this.execute(request);
  }

  getAll() {
    const request = this.imcService.getAll().pipe(
      map((imcs: ImcDto[]) => {
        return imcs.map((imc: ImcDto) => {
          return new ImcCardDto(imc);
        });
      })
    );
    this.execute(request);
  }
}
