import { inject, Injectable } from '@angular/core';
import { environment } from '@env/environment.development';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '@shared/services/auth.service';
import { FilterImcDto } from '@core/dtos/filter-imc-dto';
import { ImcDto } from '@core/dtos/imc.dto';

@Injectable({
  providedIn: 'root',
})
export class ImcService {
  private readonly API_URL: string = environment.apiBaseUrl + '/imcs';
  private http: HttpClient = inject(HttpClient);
  private readonly authService: AuthService = inject(AuthService);

  search(filterImc: FilterImcDto) {
    return this.http.get<ImcDto[]>(
      `${this.API_URL}/search?title=${filterImc.recipe}&imcType=${filterImc.imcType}&riskType=${filterImc.riskType}`,
      this.getToken()
    );
  }

  getAll() {
    return this.http.get<ImcDto[]>(`${this.API_URL}`, this.getToken());
  }

  private getToken() {
    return {
      headers: {
        Authorization: `Bearer ${this.authService.getToken()}`,
      },
    };
  }
}
