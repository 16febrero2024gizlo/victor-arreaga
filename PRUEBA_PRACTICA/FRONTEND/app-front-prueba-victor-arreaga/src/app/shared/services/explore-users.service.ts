import { inject, Injectable } from '@angular/core';

import { Service } from '@shared/services/service';
import { ImcService } from '@shared/services/imc.service';
import { FilterImcDto } from '@core/dtos/filter-imc-dto';
import { UserImcCardDto } from '@core/dtos/user-imc-card.dto';
import { map } from 'rxjs';
import { UserDto } from '@core/dtos/user.dto';
import {UserService} from "@shared/services/user.service";

@Injectable({
  providedIn: 'root',
})
export class ExploreUsersService extends Service<UserImcCardDto[]> {
  private readonly userService: UserService = inject(UserService);

  constructor() {
    super([] as UserImcCardDto[]);
  }

  get totalCards(): number {
    return this.cards.length;
  }

  get cards() {
    return this.result();
  }
  search(filterImc: FilterImcDto): void {
    const request = this.userService.searchUsers(filterImc).pipe(
      map((imcs: UserDto[]) => {
        return imcs.map((imc: UserDto) => {
          return new UserImcCardDto(imc);
        });
      })
    );
    this.execute(request);
  }

  getAll() {
    const request = this.userService.getAllUsers().pipe(
      map((imcs: UserDto[]) => {
        return imcs.map((imc: UserDto) => {
          return new UserImcCardDto(imc);
        });
      })
    );
    this.execute(request);
  }
}
