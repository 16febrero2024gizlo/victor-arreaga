import { Routes } from '@angular/router';
import { RoutePage } from '@core/enums/route-page';
import { userIsAuthenticated } from '@shared/guards/auth.guard';
import {
  roleIsOnlyAuthenticated,
  userGrantedAccess,
} from '@shared/guards/role.guard';

export const routes: Routes = [
  {
    path: RoutePage.Auth,
    loadChildren: () => import('@pages/auth/auth.routes'),
  },
  {
    path: RoutePage.Home,
    loadComponent: () => import('@pages/explore-imc/explore-imc.page'),
    canActivate: [userIsAuthenticated, roleIsOnlyAuthenticated],
  },
  {
    path: RoutePage.Home,
    loadComponent: () => import('@pages/explore-imc/explore-imc.page'),
    canActivate: [userIsAuthenticated, roleIsOnlyAuthenticated],
  },

  {
    path: '**',
    redirectTo: '',
  },
];
