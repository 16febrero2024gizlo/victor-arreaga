import { AfterViewInit, Component, inject } from '@angular/core';
import { FormGroup, ReactiveFormsModule } from '@angular/forms';
import { FilterImcDto } from '@core/dtos/filter-imc-dto';
import { ButtonType } from '@core/enums/button-type';
import { Icon } from '@core/enums/icon';
import { getAllValues, getKey } from '@core/utils/enum.util';
import { ImcTypeEnum } from '@core/enums/imc-type-enum';
import { RiskTypeEnum } from '@core/enums/risk-type-enum';
import { ExploreImcService } from '@shared/services/explore-imc.service';
import { ButtonComponent } from '@shared/components/button/button.component';
import { InputComponent } from '@shared/components/input/input.component';
import { SelectComponent } from '@shared/components/select/select.component';
import { ImcCardComponent } from '@pages/explore-imc/components/imc-card/imc-card.component';
import { toDto } from '@core/utils/form.util';

@Component({
  selector: 'app-explore-imc',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    ButtonComponent,
    InputComponent,
    SelectComponent,
    ImcCardComponent,
  ],
  templateUrl: './explore-imc.page.html',
  styles: ``,
})
export default class ExploreImcPage implements AfterViewInit {
  formGroup: FormGroup;
  service: ExploreImcService;

  private readonly filterImcDto: FilterImcDto;
  protected readonly Icon = Icon;
  protected readonly ButtonType = ButtonType;
  protected readonly getAllValues = getAllValues;
  protected readonly RiskType = RiskTypeEnum;
  protected readonly ImcType = ImcTypeEnum;

  constructor() {
    this.formGroup = new FormGroup({});
    this.filterImcDto = {} as FilterImcDto;
    this.filterImcDto.imcType = ImcTypeEnum.Normal;
    this.filterImcDto.riskType = RiskTypeEnum.Average;
    this.filterImcDto.recipe = '';
    this.service = inject(ExploreImcService);
  }

  ngAfterViewInit() {
    this.formGroup.valueChanges.subscribe(() => {
      const filterImcDto = toDto<FilterImcDto>(this.formGroup.value);
      this.filterImcDto.imcType = getKey(
        ImcTypeEnum,
        filterImcDto.imcType
      ) as ImcTypeEnum;

      this.filterImcDto.riskType = getKey(
        RiskTypeEnum,
        filterImcDto.riskType
      ) as RiskTypeEnum;

      this.filterImcDto.recipe = filterImcDto.recipe;
      this.service.search(this.filterImcDto);
    });
  }

  getResultLabel() {
    const totalArticlesCards = this.service.totalImcCards;
    if (totalArticlesCards == 0) {
      return 'No existen imc';
    }
    if (totalArticlesCards == 1) {
      return '1 imc';
    }
    return `${totalArticlesCards} imcs`;
  }
}
