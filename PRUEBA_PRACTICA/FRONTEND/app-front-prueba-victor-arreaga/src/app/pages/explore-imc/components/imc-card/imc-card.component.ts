import { Component, Input } from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';

import { Icon } from '@core/enums/icon';
import { ButtonComponent } from '@shared/components/button/button.component';
import { ButtonType } from '@core/enums/button-type';
import { ImcCardDto } from '@core/dtos/imc-card.dto';

@Component({
  selector: 'app-imc-card',
  standalone: true,
  imports: [CommonModule, ButtonComponent, NgOptimizedImage, ButtonComponent],
  templateUrl: './imc-card.component.html',
})
export class ImcCardComponent {
  @Input() imcCard: ImcCardDto;
  protected readonly Icon = Icon;
  protected readonly ButtonType = ButtonType;

  constructor() {
    this.imcCard = {} as ImcCardDto;
  }
}
