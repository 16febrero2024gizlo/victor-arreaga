import { AfterViewInit, Component, inject } from '@angular/core';
import { ButtonComponent } from '@shared/components/button/button.component';
import { ImcCardComponent } from '@pages/explore-imc/components/imc-card/imc-card.component';
import { InputComponent } from '@shared/components/input/input.component';
import { SelectComponent } from '@shared/components/select/select.component';
import { FormGroup, ReactiveFormsModule } from '@angular/forms';
import { ExploreImcService } from '@shared/services/explore-imc.service';
import { FilterImcDto } from '@core/dtos/filter-imc-dto';
import { Icon } from '@core/enums/icon';
import { ButtonType } from '@core/enums/button-type';
import { getAllValues, getKey } from '@core/utils/enum.util';
import { RiskTypeEnum } from '@core/enums/risk-type-enum';
import { ImcTypeEnum } from '@core/enums/imc-type-enum';
import { ExploreUsersService } from '@shared/services/explore-users.service';
import { toDto } from '@core/utils/form.util';

@Component({
  selector: 'app-explore-users',
  standalone: true,
  imports: [
    ButtonComponent,
    ImcCardComponent,
    InputComponent,
    SelectComponent,
    ReactiveFormsModule,
  ],
  templateUrl: './explore-users.page.html',
  styles: ``,
})
export class ExploreUsersPage implements AfterViewInit {
  formGroup: FormGroup;
  service: ExploreUsersService;

  private readonly filterImcDto: FilterImcDto;
  protected readonly Icon = Icon;
  protected readonly ButtonType = ButtonType;
  protected readonly getAllValues = getAllValues;
  protected readonly RiskType = RiskTypeEnum;
  protected readonly ImcType = ImcTypeEnum;

  constructor() {
    this.formGroup = new FormGroup({});
    this.filterImcDto = {} as FilterImcDto;
    this.filterImcDto.imcType = ImcTypeEnum.Normal;
    this.filterImcDto.riskType = RiskTypeEnum.Average;
    this.filterImcDto.recipe = '';
    this.service = inject(ExploreUsersService);
  }

  ngAfterViewInit() {
    this.formGroup.valueChanges.subscribe(() => {
      const filterImcDto = toDto<FilterImcDto>(this.formGroup.value);
      this.filterImcDto.imcType = getKey(
        ImcTypeEnum,
        filterImcDto.imcType
      ) as ImcTypeEnum;

      this.filterImcDto.riskType = getKey(
        RiskTypeEnum,
        filterImcDto.riskType
      ) as RiskTypeEnum;

      this.filterImcDto.recipe = filterImcDto.recipe;
      this.service.search(this.filterImcDto);
    });
  }

  getResultLabel() {
    const totalArticlesCards = this.service.totalCards;
    if (totalArticlesCards == 0) {
      return 'No existen imc';
    }
    if (totalArticlesCards == 1) {
      return '1 imc';
    }
    return `${totalArticlesCards} imcs`;
  }
}
