import { Component, inject, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { Icon } from '@core/enums/icon';
import { SelectComponent } from '@shared/components/select/select.component';
import { InputComponent } from '@shared/components/input/input.component';
import { ButtonComponent } from '@shared/components/button/button.component';
import { ButtonType } from '@core/enums/button-type';
import { Validators } from '@angular/forms';
import { ImcService } from '@shared/services/imc.service';

@Component({
  selector: 'app-edit-recipes',
  standalone: true,
  imports: [
    CommonModule,
    ButtonComponent,
    SelectComponent,
    InputComponent,
    AngularSvgIconModule,
    SelectComponent,
    InputComponent,
    ButtonComponent,
  ],
  templateUrl: './edit-recipes.component.html',
})
export class EditRecipesComponent {
  protected readonly Icon = Icon;
  protected readonly ButtonType = ButtonType;
  private readonly imcService: ImcService = inject(ImcService);
  errorMessage: string = '';

  @Input() recipe!: string;
  @Input() username!: string;
  showMessage!: boolean;

  onSave() {
    console.log('onSave');
  }

  existsErrorMessage() {
    return this.errorMessage !== '';
  }

  protected readonly Validators = Validators;
}
