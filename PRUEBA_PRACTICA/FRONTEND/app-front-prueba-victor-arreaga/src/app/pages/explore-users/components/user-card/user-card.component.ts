import { Component, inject, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Icon } from '@core/enums/icon';
import { ParseDate } from '@core/utils/parse-date';
import { HeaderDetailComponent } from '@shared/components/header-detail/header-detail.component';
import { ButtonComponent } from '@shared/components/button/button.component';
import { ButtonType } from '@core/enums/button-type';
import { HeaderDetail } from '@core/models/header-detail';
import { AuthService } from '@shared/services/auth.service';
import { UserDto } from '@core/dtos/user.dto';
import { EditRecipesComponent } from '@pages/explore-users/components/edit-recipes/edit-recipes.component';

@Component({
  selector: 'app-user-card',
  standalone: true,
  imports: [
    CommonModule,
    ButtonComponent,
    HeaderDetailComponent,
    HeaderDetailComponent,
    ButtonComponent,
    EditRecipesComponent,
  ],
  templateUrl: './user-card.component.html',
})
export class UserCardComponent implements OnInit {
  protected readonly ButtonType = ButtonType;
  protected readonly Icon = Icon;

  showEditRoles: boolean;
  headerDetail: HeaderDetail;
  authService: AuthService;
  @Input() user!: UserDto;

  constructor() {
    this.showEditRoles = false;
    this.headerDetail = {} as HeaderDetail;
    this.authService = inject(AuthService);
  }

  ngOnInit(): void {
    this.headerDetail = {
      photo: this.user.image,
      title: this.user.firstName.concat(' ', this.user.lastName),
      description: 'Registrado '.concat(
        ParseDate.toRelativeTime(this.user.createdAt)
      ),
    } as HeaderDetail;
  }

  onToggleShowEditRoles() {
    this.showEditRoles = !this.showEditRoles;
  }

  onDeleteUser() {
    console.log('onDeleteUser');
  }
}
