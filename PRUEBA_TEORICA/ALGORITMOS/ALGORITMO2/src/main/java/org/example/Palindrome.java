package org.example;



public class Palindrome {
    public static void main(String[] args) {
        new Palindrome().run();
    }

    private void run() {
        Console console = Console.getInstance();
        String word = console.readString("Ingrese una palabra: ");
        if (isPalindrome(word)) {
            console.write("La palabra es un palíndromo");
        } else {
            console.write("La palabra no es un palíndromo");
        }
    }

    private boolean isPalindrome(String word) {
        boolean isPalindrome = true;
        int length = word.length();
        for (int i = 0; i < length / 2; i++) {
            int lastPosition = length - i - 1;
            // Comparar caracteres en posiciones opuestas
            if (!areCharactersEqual(word, i,  lastPosition)) {
                isPalindrome = false;
                break;
            }
        }
        return isPalindrome;
    }

    private boolean areCharactersEqual(String word, int position1, int position2) {
        return word.charAt(position1) == word.charAt(position2);
    }

}