package org.example;

import java.util.ArrayList;
import java.util.List;

public class ValidateDni {

    public static void main(String[] args) {
        new ValidateDni().run();
    }

    private void run() {
        Console console = Console.getInstance();
        String dni = console.readString("Enter DNI: ");
        final int MAX_LENGTH = 10;
        if (dni.length() == MAX_LENGTH) {
            List<Integer> dniDigits = convertDniToDigits(dni);
            if (isValidDigits(dniDigits)) {
                this.checkDni(dniDigits);
            } else {
                showInvalidDni();
            }
        } else {
            showInvalidDni();
        }
    }

    private void showInvalidDni() {
        Console.getInstance().writeln("DNI incorrecto");
    }

    private void checkDni(List<Integer> dniDigits) {
        boolean isOdd;
        int coefficient;
        int sumAllDigits = 0;
        int lastDigit = dniDigits.getLast();
        for (int i = 0; i < dniDigits.size() - 1 ; i++) {
            isOdd = i % 2 == 0;
            if (isOdd) {
                coefficient = 2;
            } else {
                coefficient = 1;
            }
            if (i != dniDigits.size() - 1) {
                int digit = dniDigits.get(i);
                int value = digit * coefficient;
                if (value > 9) {
                    value -= 9;
                }
                sumAllDigits += value;
            }
        }
        int decrement = 30 - sumAllDigits;
        if (decrement == lastDigit) {
            Console.getInstance().writeln("Valid DNI");
        } else {
            showInvalidDni();
        }
    }

    private boolean isValidDigits(List<Integer> dniDigits) {
        return isValidFirstTwoDigits(dniDigits) && isThirdDigitMinorThan6(dniDigits);
    }

    private boolean isThirdDigitMinorThan6(List<Integer> dniDigits) {
        final int MAX_VALUE = 6;
        int number = dniDigits.get(2);
        return number <= MAX_VALUE;
    }

    private boolean isValidFirstTwoDigits(List<Integer> dniDigits) {
        final int MAX_VALUE = 24;
        final int MIN_VALUE = 0;
        int firstNumber = dniDigits.get(0);
        int secondNumber = dniDigits.get(1);
        int sum = firstNumber + secondNumber;
        return sum >= MIN_VALUE && sum <= MAX_VALUE;
    }

    private List<Integer> convertDniToDigits(String dni) {
        List<Integer> dniDigits = new ArrayList<>();
        for (char c : dni.toCharArray()) {
            if (Character.isDigit(c)) {
                dniDigits.add(Character.getNumericValue(c));
            }
        }
        return dniDigits;
    }
}